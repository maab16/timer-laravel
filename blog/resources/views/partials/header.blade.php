 <div class="container">
    <div class="navbar-header">
        <!-- responsive nav button -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <!-- /responsive nav button -->
        
        <!-- logo -->
        <div class="navbar-brand">
            <a href="index.html" >
                <img src="{{asset('timer/images/logo.png')}}" alt="">
            </a>
        </div>
        <!-- /logo -->
    </div>
    <!-- main menu -->
    <nav class="collapse navbar-collapse navbar-right" role="navigation">
        <div class="main-menu">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{url('/')}}" >Home</a>
                </li>
                <li><a href="{{url('/about')}}">About</a></li>
                <li><a href="{{url('/service')}}">Service</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
                    <div class="dropdown-menu">
                        <ul>
                            <li><a href="{{url('/404')}}">404 Page</a></li>
                            <li><a href="{{url('/gallery')}}">Gallery</a></li>
                        </ul>
                    </div>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog <span class="caret"></span></a>
                    <div class="dropdown-menu">
                        <ul>
                            <li><a href="{{url('/blog-fullwidth')}}">Blog Full</a></li>
                            <li><a href="{{url('/blog-left-sidebar')}}">Blog Left sidebar</a></li>
                            <li><a href="{{url('blog-right-sidebar')}}">Blog Right sidebar</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="{{url('/contact')}}">Contact</a></li>
            </ul>
        </div>
    </nav>
    <!-- /main nav -->
</div>