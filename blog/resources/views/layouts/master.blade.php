<!DOCTYPE html>
<html class="no-js">
    <head>
        @include('partials.head')
    </head>
    <body>
        <!--
        ==================================================
        Header Section Start
        ================================================== -->
        <header id="top-bar" class="navbar-fixed-top animated-header">
           @include('partials.header')
        </header>
        
        <!--
        ==================================================
        Slider Section Start
        ================================================== -->
        @yield('content')
            <!--
            ==================================================
            Footer Section Start
            ================================================== -->
            <footer id="footer">
                @include('partials.footer')
            </footer> <!-- /#footer -->
                
        </body>
    </html>